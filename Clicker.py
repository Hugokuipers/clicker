import pyautogui
pyautogui.FAILSAFE = False
import time
import threading
import pynput
import sys

position = pyautogui.position()
posX, posY = pyautogui.position()
size = pyautogui.size()
sizX, sizY = pyautogui.size()
exit = threading.Event()
suspended = threading.Event()
suspended.set()
manuallySuspended = True
mouseListen = threading.Event()
locationLocated = False

def locateLocations():
    global locationLocated
    print('finding locations')
    locateLocations.buildingOne = pyautogui.locateCenterOnScreen('imgs/farm.png')
    if locateLocations.buildingOne:
        locateLocations.buildingDistance = pyautogui.locateOnScreen('imgs/farm.png')[3]
        locationLocated = True
        print('locations found')
    else:
        locationLocated = False
        print('locations were not found')


class timeListenerThread (threading.Thread):
    def __init__(self):
        threading.Thread.__init__(self)

    def run(self):
        print('running time listener')
        runTime = 0
        while not exit.isSet():
            while not suspended.isSet():
                if locationLocated:
                    if not mouseListen.isSet():
                        mouseListen.set()
                        mouseController.click(locateLocations.buildingOne[0], locateLocations.buildingOne[1] + locateLocations.buildingDistance * runTime)
                        mouseListen.clear()
                    else:
                        print(locateLocations.buildingOne[0], locateLocations.buildingOne[1] + locateLocations.buildingDistance * runTime)
                        mouseController.click(locateLocations.buildingOne[0], locateLocations.buildingOne[1] + locateLocations.buildingDistance * runTime)
                    if runTime > 9:
                        runTime = 0
                runTime += 1
                exit.wait(5)


class keyboardListenerThread (threading.Thread):
    def __init__(self):
        threading.Thread.__init__(self)

    def run(self):
        print('running key listener')
        with pynput.keyboard.Listener(on_release=self.onRelease) as keyboardListener:
            keyboardListener.join()

    def onRelease(self, key):
        global manuallySuspended
        print('You pressed {0}'.format(key))
        if key == pynput.keyboard.Key.esc:
            print('start exitting')
            suspended.set()
            resumeTimer.cancel()
            exit.set()
            manuallySuspended = True
            mouseListen.set()
            pyautogui.click(sizX/2, sizY/2)
            return False
        elif key == pynput.keyboard.Key.f6:
            print("Start/Resume activity")
            if not locationLocated:
                locateLocations()
                exit.wait(5)
            manuallySuspended = False
            mouseController.resume()
        elif key == pynput.keyboard.Key.f7:
            print("Suspend activity")
            manuallySuspended = True
            mouseController.suspend()
            resumeTimer.cancel()
        elif key == pynput.keyboard.Key.f8:
            print("start/stop mouse move listener")
            if mouseListen.isSet():
                mouseListen.clear()
            else:
                mouseListen.set()
        elif key == pynput.keyboard.Key.f9:
            locateLocations()


class keyboardControllerThread (threading.Thread):
    def __init__(self):
        threading.Thread.__init__(self)

    def run(self):
        print('running key controller')
        while not exit.isSet():
            pass

    def press(self, key):
        print('Pressing {0}'.format(key))
        pyautogui.press(key)


class mouseMoveListenerThread (threading.Thread):
    def __init__(self):
        threading.Thread.__init__(self)

    def run(self):
        print('running mouse move listener')
        while not exit.isSet():
            while not mouseListen.isSet() and not manuallySuspended:
                prevPos = pyautogui.position()
                exit.wait(0.01)
                curPos = pyautogui.position()
                if prevPos != curPos and curPos != (sizX/2, sizY/2):
                    x, y = curPos
                    self.onMove(x, y)

    def onMove(self, x, y):
        global resumeTimer
        print('On the move, suspending activity')
        mouseController.suspend()
        if resumeTimer.is_alive():
            resumeTimer.cancel()
        resumeTimer = threading.Timer(10.0, mouseController.resume)
        resumeTimer.start()


class mouseClickListenerThread (threading.Thread):
    def __init__(self):
        threading.Thread.__init__(self)

    def run(self):
        print('running mouse click listener')
        with pynput.mouse.Listener(on_click=self.onClick) as mouseClickListener:
            mouseClickListener.join()

    def onClick(self, x, y, button, pressed):
        print('click')
        if exit.isSet():
            return False


class mouseControllerThread (threading.Thread):
    def __init__(self):
        threading.Thread.__init__(self)

    def run(self):
        print('running mouse controller')
        timesClicked = 0
        while not exit.isSet():
            while not suspended.isSet():
                self.click(sizX/2, sizY/2)
                timesClicked += 1

    def click(self, x, y):
        pyautogui.click(x,y, clicks=10)

    def suspend(self):
        suspended.set()

    def resume(self):
        suspended.clear()


threads = []

timeListener = timeListenerThread()
keyboardController = keyboardControllerThread()
keyboardListener = keyboardListenerThread()
mouseController = mouseControllerThread()
mouseMoveListener = mouseMoveListenerThread()
mouseClickListener = mouseClickListenerThread()

timeListener.start()
keyboardController.start()
keyboardListener.start()
mouseController.start()
mouseMoveListener.start()
mouseClickListener.start()

resumeTimer = threading.Timer(10.0, mouseController.resume)

threads.append(timeListener)
threads.append(keyboardController)
threads.append(keyboardListener)
threads.append(mouseController)
threads.append(mouseMoveListener)
threads.append(mouseClickListener)

for thread in threads:
    thread.join()
    print(thread)
sys.exit("it's over")
